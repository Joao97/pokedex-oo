import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class AdicionaPokemon extends JInternalFrame {
	private JTextField textField;
	private JTextField textField_1;
	Treinador auxTreinador = new Treinador();


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdicionaPokemon frame = new AdicionaPokemon(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public AdicionaPokemon(Pokedex pokedex) {
		setClosable(true);
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 615, 135);
		getContentPane().add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(244, 35, 233, 28);
		panel.add(textField);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 135, 615, 113);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		
		JLabel lbTreinador = new JLabel("Insira o nome de usu\u00E1rio do Treinador");
		lbTreinador.setBounds(26, 40, 222, 16);
		panel.add(lbTreinador);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String resultado = "";
					if(pokedex.pesquisarTreinador(textField.getText(), auxTreinador) == true) {
						panel_1.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Treinador n�o foi encontrado!");
					}
			}
		});
		button.setBounds(321, 85, 107, 23);
		panel.add(button);
		
		textField_1 = new JTextField();
		textField_1.setBounds(245, 30, 231, 30);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JButton button_1 = new JButton("Pesquisar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon auxPokemon = new Pokemon();
				String resultado = "";
				if(isNumeric(textField_1.getText())) {
					if(pokedex.pesquisaPokemon(Integer.parseInt(textField_1.getText()), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						int escolha = JOptionPane.showConfirmDialog(null, resultado, "Deseja adicionar este Pokemon?", JOptionPane.YES_NO_OPTION);
						if(escolha == JOptionPane.YES_OPTION) {
							pokedex.alterarTreinador(auxPokemon, auxTreinador.getNomeDeUsuario());
						}
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon n�o foi encontrado!");
					}
				}else {
					if(pokedex.pesquisaPokemon(textField_1.getText(), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						int escolha = JOptionPane.showConfirmDialog(null, resultado, "Deseja adicionar este Pokemon?", JOptionPane.YES_NO_OPTION);
						if(escolha == JOptionPane.YES_OPTION) {
							pokedex.alterarTreinador(auxPokemon, auxTreinador.getNomeDeUsuario());
						}
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon n�o foi encontrado!");
					}
				}
			}
		});
		button_1.setBounds(321, 71, 106, 23);
		panel_1.add(button_1);
		
		JLabel lblInsiraONome = new JLabel("Insira o nome do Pokemon");
		lblInsiraONome.setBounds(26, 43, 169, 14);
		panel_1.add(lblInsiraONome);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 248, 615, 163);
		getContentPane().add(panel_2);
		
		

	}
	
	public String montaResultado(Pokemon auxPokemon, String resultado) {
		resultado = "<html><body>ID do Pokemon: " + auxPokemon.getId() + "<br>Nome do Pokemon: " + auxPokemon.getNome() +
		"<br>Tipo 1: " + auxPokemon.getTipo1();
		if(auxPokemon.getTipo2() != null) {
			resultado += "<br>Tipo 2: " + auxPokemon.getTipo2();
		}
		resultado += "<br>Gera��o: " + auxPokemon.getGeracao() + "�";
		if(auxPokemon.isLendario()) {
			resultado += "<br>Lend�rio: Sim";
		}else {
			resultado += "<br>Lend�rio: N�o";
		}
		resultado += "<br>Habilidade 1: " + auxPokemon.getHabilidade1();
		if(auxPokemon.getHabilidade2() != null) {
			resultado += "<br>Habilidade 2: " + auxPokemon.getHabilidade2();
			if(auxPokemon.getHabilidade3() != null) {
				resultado += "<br>Habilidade 3: " + auxPokemon.getHabilidade3();
			}
		}
		resultado += "<br>Peso: " + auxPokemon.getPeso() + "<br>Altura: " + auxPokemon.getAltura();
		resultado += "<br>Pontos de Vida: " + auxPokemon.getPontosDeVida() + "<br>Ataque: " + auxPokemon.getAtaque();
		resultado += "<br>Defesa: " + auxPokemon.getDefesa() + "<br>Velocidade: " + auxPokemon.getVelocidade();
		resultado += "<br>Ataque Especial: " + auxPokemon.getAtaqueEspecial() + "<br>Defesa Especial: " + auxPokemon.getDefesaEspecial();
		return resultado;
	}


	public static boolean isNumeric(String str) {  
		try {  
		    double d = Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
		    return false;  
		}  
		  return true;  
	}
}
