import java.util.ArrayList;

public class Treinador extends Pessoa {
	
	private String nomeDeUsuario;
	ArrayList<Pokemon> pokemons;
	
	public Treinador() {
		this.pokemons = new ArrayList<Pokemon>();
	}

	public ArrayList<Pokemon> getPokemons() {
		return pokemons;
	}
	
	public Pokemon getPokemons(int index) {
		return pokemons.get(index);
	}

	public void setPokemons(Pokemon pokemon) {
		this.pokemons.add(pokemon);
	}

	public String getNomeDeUsuario() {
		return nomeDeUsuario;
	}

	public void setNomeDeUsuario(String nomeDeUsuario) {
		this.nomeDeUsuario = nomeDeUsuario;
	}	


}
