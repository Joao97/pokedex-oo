import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Label;

public class PesquisaPokemon extends JInternalFrame {
	private final JPanel panel = new JPanel();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PesquisaPokemon frame = new PesquisaPokemon(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PesquisaPokemon(Pokedex pokedex) {
		getContentPane().setBackground(SystemColor.menu);
		setTitle("Pesquisa por nome/id\r\n");
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		panel.setBounds(0, 0, 629, 140);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(223, 68, 186, 28);
		panel.add(textField);
		
		JLabel label = new JLabel("Insira o nome ou id do Pokemon");
		label.setBounds(223, 40, 186, 16);
		panel.add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 139, 629, 276);
		getContentPane().add(scrollPane);
		
		JLabel lbResultado = new JLabel();
		scrollPane.setColumnHeaderView(lbResultado);
		lbResultado.setVisible(false);
		lbResultado.setBackground(SystemColor.activeCaption);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon auxPokemon = new Pokemon();
				String resultado = "";
				if(isNumeric(textField.getText())) {
					if(pokedex.pesquisaPokemon(Integer.parseInt(textField.getText()), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						lbResultado.setText(resultado);
						lbResultado.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon n�o foi encontrado!");
					}
				}else {
					if(pokedex.pesquisaPokemon(textField.getText(), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						lbResultado.setText(resultado);
						lbResultado.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon n�o foi encontrado!");
					}
				}
				
			}
		});
		button.setBounds(271, 108, 90, 28);
		panel.add(button);		
		
	}
	
	public String montaResultado(Pokemon auxPokemon, String resultado) {
		resultado = "<html><body>ID do Pokemon: " + auxPokemon.getId() + "<br>Nome do Pokemon: " + auxPokemon.getNome() +
		"<br>Tipo 1: " + auxPokemon.getTipo1();
		if(auxPokemon.getTipo2() != null) {
			resultado += "<br>Tipo 2: " + auxPokemon.getTipo2();
		}
		resultado += "<br>Gera��o: " + auxPokemon.getGeracao() + "�";
		if(auxPokemon.isLendario()) {
			resultado += "<br>Lend�rio: Sim";
		}else {
			resultado += "<br>Lend�rio: N�o";
		}
		resultado += "<br>Habilidade 1: " + auxPokemon.getHabilidade1();
		if(auxPokemon.getHabilidade2() != null) {
			resultado += "<br>Habilidade 2: " + auxPokemon.getHabilidade2();
			if(auxPokemon.getHabilidade3() != null) {
				resultado += "<br>Habilidade 3: " + auxPokemon.getHabilidade3();
			}
		}
		resultado += "<br>Peso: " + auxPokemon.getPeso() + "<br>Altura: " + auxPokemon.getAltura();
		resultado += "<br>Pontos de Vida: " + auxPokemon.getPontosDeVida() + "<br>Ataque: " + auxPokemon.getAtaque();
		resultado += "<br>Defesa: " + auxPokemon.getDefesa() + "<br>Velocidade: " + auxPokemon.getVelocidade();
		resultado += "<br>Ataque Especial: " + auxPokemon.getAtaqueEspecial() + "<br>Defesa Especial: " + auxPokemon.getDefesaEspecial();
		return resultado;
	}


	public static boolean isNumeric(String str) {  
		try {  
		    double d = Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
		    return false;  
		}  
		  return true;  
	}
}