import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LerCSV {
	private static String arquivoCSV = "src\\POKEMONS_DATA.csv";
    private static BufferedReader br = null;
    private static String linha = "";
    private static String divisorCSV = ",";
	
	public LerCSV (ArrayList<Pokemon> pokemons) {
		try {
			br = new BufferedReader(new FileReader(arquivoCSV));
			while((linha = br.readLine()) != null) {
				Pokemon pokemon = new Pokemon();
				String[] linhaPokemon = linha.split(divisorCSV);
	            pokemon.setId(Integer.parseInt(linhaPokemon[0]));
	            pokemon.setNome(linhaPokemon[1]);
	            pokemon.setTipo1(linhaPokemon[2]);
	            pokemon.setTipo2(linhaPokemon[3]);
	            pokemon.setPontosDeVida(Integer.parseInt(linhaPokemon[4]));
	            pokemon.setAtaque(Integer.parseInt(linhaPokemon[5]));
	            pokemon.setDefesa(Integer.parseInt(linhaPokemon[6]));
	            pokemon.setAtaqueEspecial(Integer.parseInt(linhaPokemon[7]));
	            pokemon.setDefesaEspecial(Integer.parseInt(linhaPokemon[8]));
	            pokemon.setVelocidade(Integer.parseInt(linhaPokemon[9]));
	            pokemon.setGeracao(Integer.parseInt(linhaPokemon[10]));
	            pokemon.setLendario(Boolean.parseBoolean(linhaPokemon[11]));
	            pokemon.setAltura(Integer.parseInt(linhaPokemon[12]));
	            pokemon.setPeso(Integer.parseInt(linhaPokemon[13]));
	            pokemon.setHabilidade1(linhaPokemon[14]);
	            if(linhaPokemon.length >= 16) {
	            pokemon.setHabilidade2(linhaPokemon[15]);
	            }
	            if(linhaPokemon.length >= 17) {
	            pokemon.setHabilidade3(linhaPokemon[16]);
	            }
	            
	            pokemons.add(pokemon);
			}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	        arquivoCSV = "src/POKEMONS_DATA.csv";
	        try {
				br = new BufferedReader(new FileReader(arquivoCSV));
				while((linha = br.readLine()) != null) {
					Pokemon pokemon = new Pokemon();
					String[] linhaPokemon = linha.split(divisorCSV);
		            pokemon.setId(Integer.parseInt(linhaPokemon[0]));
		            pokemon.setNome(linhaPokemon[1]);
		            pokemon.setTipo1(linhaPokemon[2]);
		            pokemon.setTipo2(linhaPokemon[3]);
		            pokemon.setPontosDeVida(Integer.parseInt(linhaPokemon[4]));
		            pokemon.setAtaque(Integer.parseInt(linhaPokemon[5]));
		            pokemon.setDefesa(Integer.parseInt(linhaPokemon[6]));
		            pokemon.setAtaqueEspecial(Integer.parseInt(linhaPokemon[7]));
		            pokemon.setDefesaEspecial(Integer.parseInt(linhaPokemon[8]));
		            pokemon.setVelocidade(Integer.parseInt(linhaPokemon[9]));
		            pokemon.setGeracao(Integer.parseInt(linhaPokemon[10]));
		            pokemon.setLendario(Boolean.parseBoolean(linhaPokemon[11]));
		            pokemon.setAltura(Integer.parseInt(linhaPokemon[12]));
		            pokemon.setPeso(Integer.parseInt(linhaPokemon[13]));
		            pokemon.setHabilidade1(linhaPokemon[14]);
		            if(linhaPokemon.length >= 16) {
		            pokemon.setHabilidade2(linhaPokemon[15]);
		            }
		            if(linhaPokemon.length >= 17) {
		            pokemon.setHabilidade3(linhaPokemon[16]);
		            }
		            
		            pokemons.add(pokemon);
				}
	        } catch (FileNotFoundException e2) {
			        e2.printStackTrace();
	        } catch (IOException e2) {
		        e2.printStackTrace();
		    }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

}
