import java.awt.EventQueue;
import java.awt.SystemColor;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class Cadastro extends JInternalFrame {
	private JTextField tfNome;
	private JTextField tfNomeUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastro frame = new Cadastro(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cadastro(Pokedex pokedex) {
		getContentPane().setBackground(SystemColor.menu);
		setTitle("Cadastro de treinadores");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setClosable(true);
		setBounds(0, 0, 631, 441);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 46, 14);
		panel.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setBounds(12, 69, 46, 14);
		panel.add(lblIdade);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(12, 126, 46, 14);
		panel.add(lblSexo);
		
		JLabel lblNomeDeUsuario = new JLabel("Nome de Usuario");
		lblNomeDeUsuario.setBounds(12, 183, 104, 14);
		panel.add(lblNomeDeUsuario);
		
		tfNome = new JTextField();
		tfNome.setBounds(22, 30, 184, 28);
		panel.add(tfNome);
		tfNome.setColumns(10);
		
		tfNomeUsuario = new JTextField();
		tfNomeUsuario.setBounds(22, 201, 142, 28);
		panel.add(tfNomeUsuario);
		tfNomeUsuario.setColumns(10);
		
		JComboBox cbSexo = new JComboBox();
		lblSexo.setLabelFor(cbSexo);
		cbSexo.setModel(new DefaultComboBoxModel(new String[] {"Selecione", "Masculino", "Feminino"}));
		cbSexo.setBounds(22, 143, 126, 28);
		panel.add(cbSexo);
		
		JSpinner spIdade = new JSpinner();
		spIdade.setModel(new SpinnerNumberModel(10, 10, 130, 1));
		spIdade.setBounds(22, 86, 65, 28);
		panel.add(spIdade);
		
		JButton btnCadastrarTreinador = new JButton("Cadastrar treinador");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!(tfNome.getText().isEmpty() == true || cbSexo.getSelectedItem().toString().equals("Selecione") || tfNomeUsuario.getText().isEmpty() == true)) {
					pokedex.cadastrarTreinador(tfNome.getText(), (Integer) spIdade.getValue() , cbSexo.getSelectedItem().toString(), tfNomeUsuario.getText());
					JOptionPane.showMessageDialog(null, "Treinador cadastrado com sucesso!");
					try {
						setClosed(true);
					} catch (PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(null, "Faltam informações no cadastro do treinador.");
				}
				
			}
		});
		btnCadastrarTreinador.setBounds(241, 363, 142, 28);
		panel.add(btnCadastrarTreinador);
	}
}
