import java.util.ArrayList;

public class Pokedex {

	private ArrayList<Pokemon> pokemons;
	private ArrayList<Treinador> treinadores;
	private LerCSV lerCsv;
	
	public Pokedex() {
		this.pokemons = new ArrayList<Pokemon>();
		this.lerCsv = new LerCSV(pokemons);
		this.treinadores = new ArrayList<Treinador>();
	}
	
	public boolean pesquisaPokemon(String nome, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(nome.equals(pokemons.get(i).getNome())) {
				pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setPontosDeVida(pokemons.get(i).getPontosDeVida());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
	            pokemon.setAtaqueEspecial(pokemons.get(i).getAtaqueEspecial());
	            pokemon.setDefesaEspecial(pokemons.get(i).getDefesaEspecial());
	            pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setGeracao(pokemons.get(i).getGeracao());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setAltura((int)pokemons.get(i).getAltura());
	            pokemon.setPeso((int)pokemons.get(i).getPeso());
	            pokemon.setHabilidade1(pokemons.get(i).getHabilidade1());
	            pokemon.setHabilidade2(pokemons.get(i).getHabilidade2());
	            pokemon.setHabilidade3(pokemons.get(i).getHabilidade3());
			}
		}
		if(pokemon.getId() != 0) {
			return true;
		}
		return false;
	}
	
	public boolean pesquisaPokemon(int id, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(id == pokemons.get(i).getId()) {
				pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setPontosDeVida(pokemons.get(i).getPontosDeVida());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
	            pokemon.setAtaqueEspecial(pokemons.get(i).getAtaqueEspecial());
	            pokemon.setDefesaEspecial(pokemons.get(i).getDefesaEspecial());
	            pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setGeracao(pokemons.get(i).getGeracao());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setAltura(pokemons.get(i).getAltura());
	            pokemon.setPeso(pokemons.get(i).getPeso());
	            pokemon.setHabilidade1(pokemons.get(i).getHabilidade1());
	            pokemon.setHabilidade2(pokemons.get(i).getHabilidade2());
	            pokemon.setHabilidade3(pokemons.get(i).getHabilidade3());
			}
		}
		if(pokemon.getId() != 0) {
			return true;
		}
		return false;
	}
	
	public boolean pesquisaTipoPokemon(String tipo, ArrayList<Pokemon> listaPokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(tipo.equals(pokemons.get(i).getTipo1())) {
				listaPokemon.add(pokemons.get(i));
			}else if(tipo.equals(pokemons.get(i).getTipo2())) {
				listaPokemon.add(pokemons.get(i));
			}
		}
		if(listaPokemon.size() != 0) {
			return true;
		}
		return false;
	}
	
	public boolean pesquisarTreinador(String nomeDeUsuario, Treinador treinador) {
		for(int i = 0; i < treinadores.size(); i++) {
			if(nomeDeUsuario.equals(treinadores.get(i).getNomeDeUsuario())) {
				treinador.setNomeDeUsuario(treinadores.get(i).getNomeDeUsuario());
				treinador.setNome(treinadores.get(i).getNome());
				treinador.setIdade(treinadores.get(i).getIdade());
				treinador.setSexo(treinadores.get(i).getSexo());
				for(int j = 0; j < treinadores.get(i).pokemons.size(); j++) {
					treinador.setPokemons(treinadores.get(i).getPokemons(j));
				}
			}
		}
		if(treinador.getNomeDeUsuario() != null) {
			return true;
		}else {
			return false;
		}
	}

	public void cadastrarTreinador(String nome, int idade, String sexo, String nomeDeUsuario) {
		Treinador treinador = new Treinador();
		treinador.setNome(nome);
		treinador.setIdade(idade);
		treinador.setSexo(sexo);
		treinador.setNomeDeUsuario(nomeDeUsuario);
		treinadores.add(treinador);
 	}
	
	public void alterarTreinador(Pokemon auxPokemon, String nomeUsuario) {
		for(int i = 0; i < treinadores.size(); i++) {
			if(nomeUsuario.equals(treinadores.get(i).getNomeDeUsuario())) {
				treinadores.get(i).setPokemons(auxPokemon);
			}
		}
	}
	
}
