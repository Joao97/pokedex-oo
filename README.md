# Pokedex
### Esta é uma Pokedex desenvolvida em Java para a disciplina de Orientação a objetos.


## Funcionalidades

### Pokemons: 
#### Pesquisar por nome ou ID  
 - O nome do Pokemon deve ser informado em inglês e a primeira letra deve ser maiúscula.<br>  
 
#### Pesquisar por Tipo  
 - O tipo do Pokemon deve ser informado em inglês e a primeira letra deve ser maiúscula.<br>  

### Treinadores:
#### Cadastrar Treinadores:
 - Deve informado o nome, idade, sexo e o nome de usuário desejado do Treinador.<br>  

#### Pesquisar Treinador:
 - Deve ser informado o nome de usuário do treinador desejado.
 
#### Adicionar Pokemon:  
 - Primeiramente deve ser informado o nome de usuário do treinador que deseja adicionar um novo pokemon.<br>  
 - Em seguida deve ser informado o nome do pokemon que foi capturado pelo treinador.<br>  

---

## Atualizações:
 - versão 1.0: Lançamento da versão inicial da pokedex. data da release: 07/11/2018 <br>
 - versão 1.1: Adicionada a correção de URL do CSV quando a pokedex era acessada pelo Ubuntu. data da release: 29/11/2018